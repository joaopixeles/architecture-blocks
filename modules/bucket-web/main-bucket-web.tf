// "https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html#canned-acls",
resource "aws_s3_bucket" "default" {
  tags   = var.tags
  bucket = var.domain_name
  # website {
  #   index_document = "index.html"
  #   error_document = "error.html"
  # }
  //= "DeletionPolicy= Retain"
}

resource "aws_s3_bucket_acl" "default" {
  bucket = aws_s3_bucket.default.id
  acl    = "public-read"
}

resource "aws_s3_bucket_website_configuration" "default" {
  bucket = aws_s3_bucket.default.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }
}

resource "aws_s3_bucket_public_access_block" "default" {
  bucket                  = aws_s3_bucket.default.id
  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}
