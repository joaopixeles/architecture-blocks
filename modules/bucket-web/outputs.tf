output "s3_bucket" {
  value = aws_s3_bucket.default
}

output "s3_bucket_config" {
  value = aws_s3_bucket_website_configuration.default
}