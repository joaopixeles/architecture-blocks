variable "name" {
  type        = string
  description = "El nombre del role es igual que el del dominio"
}
variable "tags" {
  type        = map(string)
  description = "Tags para el manejo de recursos y tener mejor control de costos"
}
