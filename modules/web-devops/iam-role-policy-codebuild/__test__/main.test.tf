// "**************** MODULO & RECURSOS REQUERIDOS ******************"
resource "aws_s3_bucket" "ex" {
  bucket = "test-devops.joaopixeles.io"
  acl    = "public-read"

  tags = {
    Name        = "Terraform Test"
    Environment = "Dev"
  }
}
module "BucketArtifact" {
  source = "../../../bucket-artifacts"

  providers = {
    aws.region = aws
  }

  domain_name           = "test-devops.joaopixeles.io"
  domain_name_principal = "joaopixeles.io"
  suffix                = "artifacts"
  tags = {
    Name        = "Terraform Test"
    Environment = "Dev"
  }
}
module "IAMRoleCodebuild" {
  source = "../../iam-role-codebuild"

  domain_name = "test-devops.joaopixeles.io"
  tags = {
    Name        = "Terraform Test"
    Environment = "Dev"
  }
}

module "IAMRolePolicyCodebuild" {
  // "---------------- MODULO A PROBAR -------------------"
  source = "../"

  iam_role_name   = module.IAMRoleCodebuild.iam_role.name
  iam_role_id     = module.IAMRoleCodebuild.iam_role.id
  s3_artifact_arn = module.BucketArtifact.s3_bucket.arn
  s3_root_arn     = aws_s3_bucket.ex.arn
}
