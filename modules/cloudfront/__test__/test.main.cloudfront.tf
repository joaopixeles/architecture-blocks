module "BucketRoot" {
  #**************** MODULO REQUERIDO ******************
  source = "../../bucket-web"

  providers = {
    aws.region : aws.dns
  }

  domain_name = "action.demosdemos.click"
  WWWBucket   = false
  tags        = {}
}
module "Certification" {
  #**************** MODULO REQUERIDO ******************
  source = "../../certificate"

  providers = {
    aws.region : aws.certificate
  }


  domain_name              = "action.demosdemos.click"
  alternative_domain_names = ["*.action.demosdemos.click"]
  domain_name_principal    = "demosdemos.click"
  ttl                      = 60
  allow_overwrite          = true
  tags                     = {}
}
module "CloudFrontCDNTest" {
  #---------------- MODULO A PROBAR -------------------
  source = "../"

  providers = {
    aws.region : aws.dns
  }

  s3_id               = module.BucketRoot.s3_bucket_config.id
  s3_website_endpoint = module.BucketRoot.s3_bucket_config.website_endpoint
  domain_name         = "action.demosdemos.click"
  certificate_arn     = module.Certification.acm_certificate_validation.certificate_arn
  tags                = {}
}

module "Route53Website" {
  #**************** MODULO REQUERIDO ******************
  source = "../../web-route53"

  domain_name            = "action.demosdemos.click"
  domain_name_principal  = "demosdemos.click"
  cloudfront_domain_name = module.CloudFrontCDNTest.cloudfront_distribution.domain_name
}

# {
#   "provider": {
#     "aws": [
#       {
#         "region": "us-east-1",
#         "alias": "certificates"
#       },
#       {
#         "region": "us-west-2",
#         "alias": "dns"
#       }
#     ]
#   },
#   "module": {
#     "BucketRoot": {
#       "//": "**************** MODULO REQUERIDO ******************",
#       "source": "../../s3-bucket-root",

#       "domain_name": "action.demosdemos.click",
#       "WWWBucket": false,
#       "tags": {}
#     },
#     "Certification": {
#       "//": "**************** MODULO REQUERIDO ******************",
#       "source": "../../../cross/acm-certificate",

#       "providers": {
#         "aws.acm_account": "aws.certificates",
#         "aws.route53_account": "aws.dns"
#       },

#       "domain_name": "action.demosdemos.click",
#       "alternative_domain_names": ["*.action.demosdemos.click"],
#       "ttl": "60",
#       "allow_overwrite": true,
#       "tags": {},
#       "domain_name_principal": "demosdemos.click"
#     },

#     "CloudFrontCDNTest": {
#       "//": "---------------- MODULO A PROBAR -------------------",
#       "source": "../",

#       "s3_id": "${module.BucketRoot.s3_bucket.id}",
#       "s3_website_endpoint": "${module.BucketRoot.s3_bucket.website_endpoint}",
#       "domain_name": "action.demosdemos.click",
#       "certificate_arn": "${module.Certification.acm_certificate_validation.certificate_arn}",
#       "tags": {}
#     }
#   }
# }
