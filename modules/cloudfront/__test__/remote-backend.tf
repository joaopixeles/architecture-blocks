terraform {
  backend "s3" {
    bucket = "terraform-serendipia-organization"
    key = "demosdemos/modules/cloudfront.tfstate"
    region = "us-east-1"

    profile                  = "serendipia"
    dynamodb_table = "terraform-serendipia-organization-locks"
    encrypt = true
  }
}

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs
provider "aws" {
  shared_config_files      = ["$HOME/.aws/config"]
  shared_credentials_files = ["$HOME/.aws/credentials"]
  profile                  = "serendipia"
}