module "BucketArtifacts" {
  # source = "git@gitlab.com:joaopixeles/architectures.git//modules/bucket-artifacts?ref=0.1.03-beta"
  source = "../"

  providers = {
    aws.region = aws.virginia
  }

  domain_name_principal = var.domain_name_principal
  domain_name           = var.domain_name
  suffix                = var.suffix
  tags                  = var.tags
}
