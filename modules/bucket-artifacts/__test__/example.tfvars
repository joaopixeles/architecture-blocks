domain_name           = "test.demosdemos.click"
domain_name_principal = "demosdemos.click"
suffix                        = "artifacts"
tags = {
    "Environment" = "DEBUG",
    "Application" = "test.demosdemos.click"
    "WhoDeployed" = "joao@serendipia.co"
}