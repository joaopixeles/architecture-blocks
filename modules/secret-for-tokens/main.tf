# Firstly create a random generated password to use in secrets.
 
resource "random_password" "password" {
  length           = 256
  special          = true
  override_special = "_%@"
}

 
# Creating a AWS secret for database master account (Masteraccoundb)
 
resource "aws_secretsmanager_secret" "secret" {
   name = "${var.sub_domian_name}-${var.endpoint_group}"
}
 
# Creating a AWS secret versions for database master account (Masteraccoundb)
 
resource "aws_secretsmanager_secret_version" "api" {
  secret_id = aws_secretsmanager_secret.secret.id
  secret_string = <<EOF
   {
    "jwt_secret": "${random_password.password.result}"
   }
EOF
}

 
# # Importing the AWS secrets created previously using arn.
 
# data "aws_secretsmanager_secret" "secret" {
#   arn = aws_secretsmanager_secret.secret.arn
# }
 
# # Importing the AWS secret version created previously using arn.
 
# data "aws_secretsmanager_secret_version" "creds" {
#   secret_id = data.aws_secretsmanager_secret.secret.arn
# }
 
# # After importing the secrets storing into Locals
 
# locals {
#   db_creds = jsondecode(data.aws_secretsmanager_secret_version.creds.secret_string)
# }

# output "example" {
#   value = jsondecode(data.aws_secretsmanager_secret_version.creds.secret_string)["api"]
# }