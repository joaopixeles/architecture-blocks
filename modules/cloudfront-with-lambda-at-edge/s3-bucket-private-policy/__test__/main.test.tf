module "CFOriginAccessIdentityTest" {
  source="../../cloudfront-origin-access-identity"

  domain_name="test.wvle.joaopixeles.io"
}

module "S3BucketPrivateTest" {
  source = "../../s3-bucket-private"

  domain_name = "test.wvle.joaopixeles.io"
}

module "S3BucketPrivatePilicyTest" {
  source = "../"

  bucket_arn = module.S3BucketPrivateTest.output_json.arn
  bucket_id = module.S3BucketPrivateTest.output_json.id
  origin_access_identity_iam_arn=module.CFOriginAccessIdentityTest.output_json.iam_arn
}