variable "s3_bucket_private_domain_name" {
  type = string
}
variable "s3_bucket_public_domain_name" {
  type = string
}
variable "origin_access_identity" {
  type = map(any)
}
variable "certificate_arn" {
  type = string
}
variable "domain_name" {
  type = string
}
variable "tags" {
  type = map(any)
}
variable "lambda_function_arn" {
  type = string
}
