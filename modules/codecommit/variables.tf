//": "acr acronym aws_codecommit_repository"
variable "repository_name" {
  description = "SubDomain name for your devops (sub.example.com)"
  type        = string
  default     = ""
}
variable "tags" {
  type        = map(string)
  description = "Tags para el manejo de recursos y tener mejor control de costos"
}
