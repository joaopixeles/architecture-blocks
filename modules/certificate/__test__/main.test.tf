module "CertificateTest" {
  source = "../"

  providers = {
    aws.region : aws.certification
  }

  domain_name              = var.domain_name
  sub_domian_name          = var.sub_domian_name  
  tld                      = var.tld
  ttl                      = var.ttl
  allow_overwrite          = var.allow_overwrite
  tags                     = var.tags
}
