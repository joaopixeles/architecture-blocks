data "aws_route53_zone" "selected" {
  name         = "${var.domain_name}.${var.tld}."
  private_zone = false
}