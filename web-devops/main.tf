module "IAMRoleCodebuild" {
  source = "../modules/web-devops/iam-role-codebuild"

  name = local.name_key # this is for standard propose.
  tags = var.tags
}
module "IAMRolePolicyCodebuild" {
  source = "../modules/web-devops/iam-role-policy-codebuild"

  iam_role_name   = module.IAMRoleCodebuild.iam_role.name
  iam_role_id     = module.IAMRoleCodebuild.iam_role.id
  s3_artifact_arn = data.aws_s3_bucket.artifacts.arn
  s3_root_arn     = data.aws_s3_bucket.root.arn
}
module "CodebuildProjectWebsite" {
  source = "../modules/web-devops/codebuild-project"

  BUCKET_NAME  = data.aws_s3_bucket.root.id
  name         = local.name_key # this is for standard propose.
  iam_role_arn = module.IAMRoleCodebuild.iam_role.arn
  tags         = var.tags
  buildspec_path = var.buildspec_path
}
module "IAMRoleCodepipeline" {
  source = "../modules/web-devops/iam-role-codepipeline"

  name = local.name_key # this is for standard propose.
  tags = var.tags
}
module "IAMRolePolicyCodepipeline" {
  source = "../modules/web-devops/iam-role-policy-codepipeline"

  iam_role_name   = module.IAMRoleCodepipeline.iam_role.name
  iam_role_id     = module.IAMRoleCodepipeline.iam_role.id
  s3_artifact_arn = data.aws_s3_bucket.artifacts.arn
}
module "CodepipelineWebsite" {
  source = "../modules/web-devops/codepipeline"

  name                   = local.name_key # in the bucket the path change with the name of the pipeline
  iam_role_arn           = module.IAMRoleCodepipeline.iam_role.arn
  repository_name        = var.repository_name
  repository_branch      = var.repository_branch
  codebuild_project_name = module.CodebuildProjectWebsite.codebuild_project.name
  s3_artifact_name       = data.aws_s3_bucket.artifacts.bucket
  tags                   = var.tags
}
