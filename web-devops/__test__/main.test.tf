module "WebsiteDevOpsTest" {
  source = "../"

  bucket_artifacts_name = var.bucket_artifacts_name
  sub_domian_name       = var.sub_domian_name
  domain_name           = var.domain_name
  tld                   = var.tld
  tags                  = var.tags
  repository_name       = var.repository_name
  repository_branch     = var.repository_branch
  bucket_webfiles       = var.bucket_webfiles
  buildspec_path        = var.buildspec_path
  type                  = var.type
}
