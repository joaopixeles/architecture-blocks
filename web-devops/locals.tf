# locals {
#   domain_subdomain   = replace(replace(var.domain_name, var.domain_name_principal, ""), ".", "")
#   domain_without_TLD = "${split(".", var.domain_name_principal)[0]}-"
#   name_key           = var.domain_name == var.domain_name_principal ? "${local.domain_without_TLD}site" : "${local.domain_without_TLD}${local.domain_subdomain}"
#   bucket_webfiles    = local.name_key
#   repository_name    = local.name_key
# }

locals {
  full_domain = var.sub_domian_name == var.domain_name ? "${var.domain_name}.${var.tld}": "${var.sub_domian_name}.${var.domain_name}.${var.tld}"
  name_key = var.sub_domian_name == var.domain_name ? "${var.type}-${var.domain_name}": "${var.type}-${var.sub_domian_name}"
  alternative_domain_names = ["*.${local.full_domain}"]
}