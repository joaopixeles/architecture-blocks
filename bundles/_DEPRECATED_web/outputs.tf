output "s3_bucket_root" {
  value = module.BucketRoot.s3_bucket
}
output "certificate" {
  value = data.aws_acm_certificate.website
}
output "route53_record" {
  value = module.Route53Website
}
output "cloudfront_distribution" {
  value = module.CloudFrontCDN
}
output "route53_record_root" {
  value = module.Route53Website
}