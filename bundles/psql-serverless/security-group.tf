################################################################################
# Security Group
################################################################################

resource "aws_security_group" "this" {
  count = var.create_cluster && var.create_security_group ? 1 : 0

  name_prefix = "${var.name}-"
  vpc_id      = var.vpc_id

  description = var.security_group_description == "" ? "Control traffic to/from RDS Aurora ${var.name}" : var.security_group_description

  tags = merge(var.tags, var.security_group_tags, {
    Name = local.name
  })
}

resource "aws_security_group_rule" "default_ingress" {
  count = var.create_cluster && var.create_security_group ? length(var.allowed_security_groups) : 0

  description = "From allowed SGs"

  type                     = "ingress"
  from_port                = element(concat(aws_rds_cluster.this.*.port, [""]), 0)
  to_port                  = element(concat(aws_rds_cluster.this.*.port, [""]), 0)
  protocol                 = "tcp"
  source_security_group_id = element(var.allowed_security_groups, count.index)
  security_group_id        = local.rds_security_group_id
}

resource "aws_security_group_rule" "cidr_ingress" {
  count = var.create_cluster && var.create_security_group && length(var.allowed_cidr_blocks) > 0 ? 1 : 0

  description = "From allowed CIDRs"

  type              = "ingress"
  from_port         = element(concat(aws_rds_cluster.this.*.port, [""]), 0)
  to_port           = element(concat(aws_rds_cluster.this.*.port, [""]), 0)
  protocol          = "tcp"
  cidr_blocks       = var.allowed_cidr_blocks
  security_group_id = local.rds_security_group_id
}