output "subnet_ids" {
  value = module.SubnetDB.subnet_ids
}
output "security_group" {
  value = module.SecurityGroupPG.security_group
}
output "db_parameter_group" {
  value = module.DBPGParameterGroup.db_parameter_group
}
output "db_subnet_group" {
  value = module.DBSubnetGroup.db_subnet_group
}
output "iam_role" {
  value = module.IAMRoleDBMonitoring.iam_role
}
output "iam_role_policy_attachment" {
  value = module.IAMRoleDBMonitoring.iam_role_policy_attachment
}
output "db_instance" {
  value       = module.Postgresql.db_instance
  description = "database outbut info"
}
output "sns_topic" {
  value = module.DBMonitoringCloudWatch.sns_topic
}
output "db_event_subscription" {
  value = module.DBMonitoringCloudWatch.db_event_subscription
}
output "sns_topic_policy" {
  value = module.DBMonitoringCloudWatch.sns_topic_policy
}
output "cloudwatch_metric_alarm_burst_balance_too_low" {
  value = module.DBMonitoringCloudWatch.cloudwatch_metric_alarm_burst_balance_too_low
}
output "cloudwatch_metric_alarm_cpu_credit_balance_too_low" {
  value = module.DBMonitoringCloudWatch.cloudwatch_metric_alarm_cpu_credit_balance_too_low
}
output "cloudwatch_metric_alarm_cpu_utilization_too_high" {
  value = module.DBMonitoringCloudWatch.cloudwatch_metric_alarm_cpu_utilization_too_high
}
output "cloudwatch_metric_alarm_disk_queue_depth_too_high" {
  value = module.DBMonitoringCloudWatch.cloudwatch_metric_alarm_disk_queue_depth_too_high
}
output "cloudwatch_metric_alarm_free_storage_space_too_low" {
  value = module.DBMonitoringCloudWatch.cloudwatch_metric_alarm_free_storage_space_too_low
}
output "cloudwatch_metric_alarm_freeable_memory_too_low" {
  value = module.DBMonitoringCloudWatch.cloudwatch_metric_alarm_freeable_memory_too_low
}
output "cloudwatch_metric_alarm_swap_usage_too_high" {
  value = module.DBMonitoringCloudWatch.cloudwatch_metric_alarm_swap_usage_too_high
}
