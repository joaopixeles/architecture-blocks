domain_name= "dev.hasura.tomate.chat"
vpc_id= "vpc-b82be2dd"
subnets= {
  A= {
    cidr_block= "172.31.255.128/26"
    availability_zone= "us-east-1a"
  }
  B= {
    cidr_block= "172.31.255.192/26"
    availability_zone= "us-east-1b"
  }
}

allocated_storage= "20"
engine_version= "12.2"
instance_type= "db.t2.micro"
storage_type= "gp2"
iops= 0
database_identifier= "non-pro-tomate-chat"
snapshot_identifier= ""
database_name= "dev_tomate_chat"
database_username= "tomatechat"
database_password= "Welcome12345#"
database_port= "5432"

backup_retention_period= "30"
backup_window= "04:00-04:30"
maintenance_window= "sun:04:30-sun:05:30"
auto_minor_version_upgrade= false
final_snapshot_identifier= "terraform-aws-postgresql-rds-snapshot"
skip_final_snapshot= true

copy_tags_to_snapshot= false
multi_availability_zone= false
storage_encrypted= false
monitoring_interval= 60
deletion_protection= false
cloudwatch_logs_exports= ["postgresql"]
tags= {
  Environment= "DEV",
  Application= "tomate.chat",
  Service= "postgresql",
  WhoDeployed= "e@serendipia.co"
}
