data "aws_security_group" "postgresql" {
  tags = {
    Environment = "${var.tags.Environment}"
    Application = "${var.tags.Application}"
    Service     = "${var.tags.DB}"
  }
}
data "aws_subnet_ids" "postgresql" {
  vpc_id = var.vpc_id
  tags = {
    Environment = "${var.tags.Environment}"
    Application = "${var.tags.Application}"
    Service     = "${var.tags.DB}"
  }
}
