module "Route53Cognito" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/web-route53?ref=0.0.01-beta"

  domain_name            = aws_cognito_user_pool_domain.custom.domain
  domain_name_principal  = var.domain_name_principal
  cloudfront_domain_name = aws_cognito_user_pool_domain.custom.cloudfront_distribution_arn
}
