resource "aws_cognito_user_pool" "default" {
  name                     = "${var.domain_sub}-userpool"
  auto_verified_attributes = ["email"]

  schema {
    attribute_data_type = "String"
    name                = "email"
    required            = true
  }
}
