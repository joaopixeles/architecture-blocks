module "LambdaAtEdgeSecurity" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/lambda-at-edge?ref=0.0.05-beta"

  providers = {
    aws = aws.virginia
  }

  execution_role_policy_name = "auth-${local.domain_name_ordered}-policy"
  execution_role_name        = "auth-${local.domain_name_ordered}-role"
  function_name              = var.function_name
  handler                    = var.handler
  runtime                    = var.runtime
  bucket_name                = var.bucket_name
  bucket_filepath            = var.bucket_filepath
}
module "CFOriginAccessIdentity" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/cloudfront-with-lambda-at-edge/cf-origin-access-identity?ref=0.0.05-beta"

  comment = local.domain_name_ordered
}
module "S3BucketPrivate" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/cloudfront-with-lambda-at-edge/s3-bucket-private?ref=0.0.05-beta"

  bucket_name = "${local.domain_name_ordered}-origin-private"
}
module "S3BucketPrivatePilicy" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/cloudfront-with-lambda-at-edge/s3-bucket-private-policy?ref=0.0.05-beta"

  bucket_arn                     = module.S3BucketPrivate.output_json.arn
  bucket_id                      = module.S3BucketPrivate.output_json.id
  origin_access_identity_iam_arn = module.CFOriginAccessIdentity.output_json.iam_arn
}
module "S3BucketPublic" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/cloudfront-with-lambda-at-edge/s3-bucket-public?ref=0.0.05-beta"

  bucket_name = "${local.domain_name_ordered}-origin-public"
}
module "S3BucketPublicPilicy" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/cloudfront-with-lambda-at-edge/s3-bucket-public-policy?ref=0.0.05-beta"

  bucket_arn                     = module.S3BucketPublic.output_json.arn
  bucket_id                      = module.S3BucketPublic.output_json.id
  origin_access_identity_iam_arn = module.CFOriginAccessIdentity.output_json.iam_arn
}
module "Route53Website" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/web-route53?ref=0.0.05-beta"

  domain_name            = var.domain_name
  domain_name_principal  = var.domain_name_principal
  cloudfront_domain_name = module.CloudFrontDistribution.output_json.domain_name
}
module "CloudFrontDistribution" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/cloudfront-with-lambda-at-edge/cf-distribution?ref=0.0.05-beta"

  domain_name                   = var.domain_name
  s3_bucket_private_domain_name = module.S3BucketPrivate.output_json.bucket_domain_name
  s3_bucket_public_domain_name  = module.S3BucketPublic.output_json.bucket_domain_name
  origin_access_identity        = module.CFOriginAccessIdentity.output_json
  certificate_arn               = data.aws_acm_certificate.webviews.arn
  tags                          = var.tags
  lambda_function_arn           = module.LambdaAtEdgeSecurity.lambda_function_arn_version
}
