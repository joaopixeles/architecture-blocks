output "CFOriginAccessIdentity" {
  value = module.CFOriginAccessIdentity.output_json
}

output "S3BucketPrivate" {
  value = module.S3BucketPrivate.output_json
}

output "S3BucketPrivatePilicy" {
  value = module.S3BucketPrivatePilicy.output_json
}

output "S3BucketPublic" {
  value = module.S3BucketPublic.output_json
}

output "S3BucketPublicPilicy" {
  value = module.S3BucketPublicPilicy.output_json
}

output "CertificationWebviews" {
  value = data.aws_acm_certificate.webviews
}

output "Route53Website" {
  value = module.Route53Website
}

output "CloudFrontDistribution" {
  value = module.CloudFrontDistribution.output_json
}
