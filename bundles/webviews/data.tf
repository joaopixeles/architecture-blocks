data "aws_acm_certificate" "webviews" {
  provider = aws.virginia
  domain = var.domain_name
}